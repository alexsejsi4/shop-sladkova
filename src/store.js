import { createStore } from 'vuex'

const isProductInCort = (cort, product) => {
  return cort.some(item => {
    return item.id === product.id
  })
}

const state = {
  cort: []
}

const mutations = {
  addToCort (state, product) {
    if (!isProductInCort(state.cort, product)) {
      product.amount = 1
      state.cort.push(product)
    }
  },
  incAmount (state, index) {
    state.cort[index].amount++
  },
  decAmount (state, index) {
    if (state.cort[index].amount > 1) {
      state.cort[index].amount--
    }
  }
}

const getters = {
  getCort: state => state.cort
}

export default createStore({
  state,
  getters,
  mutations
})
