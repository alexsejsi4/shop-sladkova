import store from './store.js'
import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import App from './components/App.vue'
import Cort from './components/Cort.vue'
import ProductInfo from './components/ProductInfo.vue'
import ProductsList from './components/ProductsList.vue'

const routes = [
  { path: '/', component: ProductsList },
  { path: '/product/:id', component: ProductInfo },
  { path: '/cort', component: Cort }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

const app = createApp(App)
app.use(store).use(router).mount('#app')
